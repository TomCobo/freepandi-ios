//
//  AppDelegate.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 28/11/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.setupWindow()
        
        self.setupAppAppearance(application)
        
        self.setupThirdPatyLibraries()
        
        CommonData.sharedData
        
        return true
    }

    
    //  MARK: - launching setup
    
    fileprivate func setupWindow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        window.makeKeyAndVisible()
        window.rootViewController = NavigationService.sharedService.initialise()
        self.window = window
    }
    
    fileprivate func setupThirdPatyLibraries() {
        Fabric.with([Crashlytics.self])
    }
    
    fileprivate func setupAppAppearance(_ application: UIApplication) {
        application.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
    }


}

