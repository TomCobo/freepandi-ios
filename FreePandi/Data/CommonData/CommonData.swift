//
//  CommonData.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

class CommonData {
    
    static let sharedData = CommonData()
    
    var quiz:Quiz?
    
    init() {
        DataManager.deserializeJSONFile("level") {
            self.quiz = Quiz(withObjects: $0)
        }
    }

}

extension CommonData: Equatable { }

func ==(lhs: CommonData, rhs: CommonData) -> Bool {
    return lhs === rhs
}
