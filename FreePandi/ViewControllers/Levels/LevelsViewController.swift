//
//  LevelsViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 21/02/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class LevelsViewController: BaseViewController {
    
    override var rootView: LevelsRootView  {
        if _rootView == nil, let quiz = self.params as? Quiz {
            let rootView = LevelsRootView(quiz:quiz)
            rootView.delegate = self
            _rootView = rootView
        }
        return _rootView as! LevelsRootView
    }
    
}

extension LevelsViewController:LevelsRootViewDelegate {
    func levelsRootViewSelect(_ stage:Stage) {
        let stageViewController = StageViewController(params: stage)
        self.navigationService.pushViewController(stageViewController)
    }
}
