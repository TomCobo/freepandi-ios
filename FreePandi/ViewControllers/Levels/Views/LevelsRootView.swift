//
//  LevelsRootView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 21/02/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

protocol LevelsRootViewDelegate:class {
    func levelsRootViewSelect(_ stage:Stage)
}

class LevelsRootView: BaseView {
    
    weak var delegate:LevelsRootViewDelegate?
    
    fileprivate lazy var levelsViewControllers:[BaseViewController]? = {
        guard let levels:Array<Level> = self.viewModel?.levels else { return nil }
        let levelsViewControllers = self.createViewControllersFrom(levels)
        return levelsViewControllers
    }()
    
    fileprivate func createViewControllersFrom(_ levels:Array<Level>)->[BaseViewController] {
        var levelsViewControllers:[BaseViewController] = []
        for level in levels {
            let levelViewController = LevelViewController(params:level)
            levelViewController.delegate = self
            levelsViewControllers.append(levelViewController)
        }
        return levelsViewControllers
    }
    
    //  MARK: - Init
    
    init(quiz:Quiz?) {
        super.init(frame:CGRect.zero)
        
        defer {
            self.viewModel = quiz
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - Subviews
    
    fileprivate lazy var levelsPageViewControllers:UIPageViewController = {
        let levelsPageViewControllers:UIPageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        levelsPageViewControllers.view.backgroundColor = FreePandiTheme.sharedManager.commonBlueA100
        levelsPageViewControllers.dataSource = self 
        return levelsPageViewControllers
    }()
    
    //  MARK: - Data
    
    fileprivate var viewModel:Quiz? {
        didSet {
            self.updateUI()
        }
    }
    
    fileprivate func  updateUI() {
        
        guard let firstLevelViewController = self.levelsViewControllers?.first else { return }
        
        self.levelsPageViewControllers.setViewControllers([firstLevelViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        
    }
    
    //  MARK: - setup layout
    
    func setupSubviews() {
        self.addSubview(self.levelsPageViewControllers.view)
    }
    
    func setupAutolayout() {
        constrain(self.levelsPageViewControllers.view) { view in
            view.center == view.superview!.center
            view.size == view.superview!.size
        }
    }
    
}

// MARK: - UIPageViewControllerDataSource

extension LevelsRootView:UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? BaseViewController
            , let index:Int = self.levelsViewControllers?.index(of: vc)
            , let numberOfLevels = self.levelsViewControllers?.count, index < (numberOfLevels - 1) else  { return nil }
        
        return self.levelsViewControllers?[index+1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? BaseViewController
            , let index:Int = self.levelsViewControllers?.index(of: vc), index > 0 else  { return nil }
        
        return self.levelsViewControllers?[index-1]
    }
    
}

// MARK: - LevelViewControllerDelegate

extension LevelsRootView:LevelViewControllerDelegate {
    
    func levelViewControllerSelect(_ stage: Stage) {
        self.delegate?.levelsRootViewSelect(stage)
    }
    
}
