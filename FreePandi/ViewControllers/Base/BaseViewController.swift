//
//  BaseViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Variables
    
    internal var params: Any?
    unowned var navigationService: NavigationService = NavigationService.sharedService
    
    internal var _rootView: BaseView?
    internal var rootView: BaseView? { return _rootView }
    
    var viewControllerType: ViewController?
    
    // MARK: - Initialisers
    
    init(params: Any) {
        super.init(nibName: nil, bundle: nil)
        self.params = params
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Controller livecycle methods
    
    override func loadView() {
        self.view = self.rootView!
    }
    
    func closeButtonTapped(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
