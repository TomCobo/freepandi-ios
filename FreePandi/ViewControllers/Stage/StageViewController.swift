//
//  StageViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class StageViewController: BaseViewController {

    override var rootView: StageRootView  {
        if _rootView == nil, let stage = self.params as? Stage {
            let rootView = StageRootView(stage:stage)
            _rootView = rootView
        }
        return _rootView as! StageRootView
    }


}
