//
//  StageRootView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

class StageRootView: BaseView {
    
    
    //  MARK: - init
    
    init(stage:Stage?) {
        super.init(frame:CGRect.zero)
        
        defer {
            self.viewModel = stage
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - subviews
    
    fileprivate lazy var collectionView: FreePandiCollectionView = {
        let collectionView = FreePandiCollectionView(withDataSource: nil, andDelegate:nil)
//        collectionView.registerClass(StageCollectionViewCell.self)
        return collectionView
    }()
    
    fileprivate lazy var attemptsLabel: UILabel = {
        let attemptsLabel:UILabel = FreePandiTheme.sharedManager.commonTitleLabel
        attemptsLabel.textAlignment = NSTextAlignment.center
        attemptsLabel.backgroundColor = FreePandiTheme.sharedManager.commonPinkA200
        return attemptsLabel
    }()
    
    //  MARK: - data
    
    fileprivate var viewModel:Stage? {
        didSet {
            self.updateUI()
        }
    }
    
    fileprivate func updateUI() {
        guard let stage = self.viewModel, let attempts = stage.attempts else { return }
        
        self.updateAttemptsTo(attempts)
    }
    
    fileprivate func updateAttemptsTo(_ numberOfAttemts:Int) {
        let attemptsSuffix = NSLocalizedString("stage_attemtps", comment: "")
        self.attemptsLabel.text = "\(numberOfAttemts)" + attemptsSuffix
    }
    
    //  MARK: - setup layout
    
    func setupSubviews() {
        self.backgroundColor = FreePandiTheme.sharedManager.commonBlueA100
        self.addSubview(self.collectionView)
        self.addSubview(self.attemptsLabel)
    }
    
    func setupAutolayout() {
//        constrain(self.collectionView) { view in
//            view.top == view.top + 30
//            view.width == view.superview!.width
//            view.bottom == view.superview!.bottom - 30
//        }
        
        constrain(self.attemptsLabel) { view in
            view.height == 30
            view.width == view.superview!.width
            view.bottom == view.superview!.bottom
        }
    }

}
