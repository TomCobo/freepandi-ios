//
//  InitialRootView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

protocol InitialRootViewDelegate:class {
    func initialRootViewTappedPlayButton()
}

class InitialRootView: BaseView {
    
    weak var delegate:InitialRootViewDelegate?
    
    //  MARK: - Free Letter vars
    
    fileprivate var animatorPandi: UIDynamicAnimator!
    fileprivate var animatorFree: UIDynamicAnimator!
    fileprivate var gravity: UIGravityBehavior?
    fileprivate var collision: UICollisionBehavior?
    fileprivate var buttonBarrierIdentifier:NSCopying = "buttonBarrier" as NSCopying
    fileprivate var pandiBarrierIdentifier:NSCopying = "pandiBarrier" as NSCopying
    
    //  MARK: - Subviews
    
    fileprivate lazy var pandiView:PandiView = {
       let pandiView = PandiView()
        pandiView.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
        return pandiView
    }()
    
    fileprivate lazy var jailView:JailView = {
        let jailView = JailView()
        return jailView
    }()
    
    fileprivate lazy var playButton:UIButton = {
        let playButton =  FreePandiTheme.sharedManager.playButton
        playButton.addTarget(self, action: #selector(InitialRootView.playGame(_:)), for: UIControlEvents.touchUpInside)
        return playButton
    }()
    
    fileprivate lazy var freeTitle:UILabel = {
        let freeTitle =  FreePandiTheme.sharedManager.commonTitleLabel
        freeTitle.text = "Free"
        return freeTitle
    }()
    
    fileprivate lazy var pandiTitle:UILabel = {
        let pandiTitle =  FreePandiTheme.sharedManager.commonTitleLabel
        pandiTitle.text = "Pandi"
        return pandiTitle
    }()
    
    func configure() {
        self.backgroundColor = FreePandiTheme.sharedManager.commonBlueA100
        self.initialPandiAndJailAnimation()
    }
    
    func setupSubviews() {
        self.addSubview(self.pandiView)
        self.addSubview(self.jailView)
        self.addSubview(self.playButton)
        self.addSubview(self.freeTitle)
        self.addSubview(self.pandiTitle)
    }
    
    func setupAutolayout() {
        
        constrain(self.pandiView) { view in
            view.centerX == view.superview!.centerX
            view.centerY == view.superview!.centerY - 50
        }
        
        constrain(self.jailView, self.pandiView) { view1, view2 in
            view1.center == view2.center
            view1.width == 260
        }
        
        constrain(self.playButton) { view in
            view.centerX == view.superview!.centerX
            view.centerY == view.superview!.centerY + 150
            view.height == view.width
            view.width == 100
        }
        
        constrain(self.pandiTitle) { view in
            view.centerX == view.superview!.centerX + 50
            view.bottom  == view.superview!.top - 200
        }
        
        constrain(self.freeTitle, self.playButton) { view,view2 in
            view.leading == view.superview!.leading - 200
            view.bottom == view2.top
        }
        
    }
    
    func playGame(_ sender:UIButton) {
        self.delegate?.initialRootViewTappedPlayButton()
    }
    
    //  MARK: - Private animations
    
    fileprivate func initialPandiAndJailAnimation() {
        
        delay(0.4) { () -> () in
            UIView.animate(withDuration: 0.6, animations: { () -> Void in
                self.pandiView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                }, completion: { [weak self] (finished) -> Void in
                    self?.closeJailAnimation()
            })
        }
    }
    
    fileprivate func closeJailAnimation() {
        self.jailView.closeJail({ [weak self] (finished) -> Void in
            if finished {
                self?.gravityAnimation()
                self?.pandiView.rotatePandiHeadRigth()
                self?.playButton.bounceAnimation(withScale: 0.95)
            }
        })
    }
    
    fileprivate func gravityAnimation() {
        
        var buttonFrame = self.playButton.frame
        buttonFrame.size = CGSize(width: DeviceUtils.screenWidth, height: buttonFrame.height)
        
        self.animatorPandi = UIDynamicAnimator(referenceView: self)
        
        self.gravity = UIGravityBehavior(items:[self.pandiTitle])
        
        self.collision = UICollisionBehavior(items: [self.pandiTitle]);
        self.collision?.collisionDelegate = self
        self.collision?.addBoundary(withIdentifier: buttonBarrierIdentifier, for: UIBezierPath(rect:buttonFrame ))
        
        self.animatorPandi.addBehavior(self.gravity!)
        self.animatorPandi.addBehavior(self.collision!)
    }
    
    fileprivate func lateralAnimation() {
        
        var pandiFrame = self.pandiTitle.frame
        pandiFrame.size = CGSize(width: pandiFrame.width + 60, height: DeviceUtils.screenHeight)
        
        self.animatorFree = UIDynamicAnimator(referenceView: self)
        
        self.gravity = UIGravityBehavior(items:[self.freeTitle])
        self.gravity?.gravityDirection = CGVector(dx: 1.0, dy: 0.0)
        
        self.collision = UICollisionBehavior(items: [self.freeTitle])
        
        self.collision?.addBoundary(withIdentifier: pandiBarrierIdentifier, for: UIBezierPath(rect: pandiFrame))
        
        self.animatorFree.addBehavior(self.gravity!)
        self.animatorFree.addBehavior(self.collision!)
    }
    

}

extension InitialRootView: UICollisionBehaviorDelegate {
    
    func collisionBehavior(_ behavior: UICollisionBehavior, endedContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?) {
        if item === self.pandiTitle {
            self.lateralAnimation()
        }
    }
    
}
