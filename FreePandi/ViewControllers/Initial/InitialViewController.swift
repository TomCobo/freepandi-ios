//
//  InitialViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

class InitialViewController: BaseViewController {

    override var rootView: InitialRootView  {
        if _rootView == nil {
            let rootView = InitialRootView()
            rootView.delegate = self
            _rootView = rootView
        }
        return _rootView as! InitialRootView
    }

}

extension InitialViewController:InitialRootViewDelegate {
    
    func initialRootViewTappedPlayButton() {
        guard let quiz:Quiz = CommonData.sharedData.quiz else { return }
        self.navigationService.presentViewController(LevelsViewController(params:quiz))
    }
    
}
