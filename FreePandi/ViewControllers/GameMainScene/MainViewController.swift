//
//  MainViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationService.presentViewController(ViewController.Initial, animated: false)
    }
    
    // MARK: - root view
    
    override var rootView: BaseView {
        if (_rootView == nil) {
            let rootView = BaseView()
            _rootView = rootView
        }
        return _rootView!
    }
    
    
}

