//
//  LevelViewController.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 06/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

protocol LevelViewControllerDelegate:class {
    func levelViewControllerSelect(_ stage:Stage)
}

class LevelViewController:BaseViewController {
    
    weak var delegate:LevelViewControllerDelegate?

    override var rootView: LevelRootView  {
        if _rootView == nil, let level = self.params as? Level {
            let rootView = LevelRootView(level:level)
            rootView.delegate = self
            _rootView = rootView
        }
        return _rootView as! LevelRootView
    }
    
}

extension LevelViewController:LevelRootViewDelegate {
    
    func levelRootDidSelect(_ stage:Stage) {
        self.delegate?.levelViewControllerSelect(stage)
    }
    
}
