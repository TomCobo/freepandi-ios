//
//  LevelRootView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 06/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

protocol LevelRootViewDelegate:class {
    func levelRootDidSelect(_ stage:Stage)
}

class LevelRootView: BaseView {
    
    weak var delegate:LevelRootViewDelegate?
    
    //  MARK: - init
    
    init(level:Level?) {
        super.init(frame:CGRect.zero)
        
        defer {
            self.viewModel = level
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - subviews

    fileprivate lazy var collectionView: FreePandiCollectionView = {
        let collectionView = FreePandiCollectionView(withDataSource: self.dataSource, andDelegate:self)
        collectionView.registerClass(LevelCollectionViewCell.self)
        return collectionView
    }()
    
    fileprivate lazy var title: UILabel = {
        let title = FreePandiTheme.sharedManager.commonTitleLabel
        return title
    }()
    
    //  MARK: - data
    
    fileprivate var viewModel:Level? {
        didSet {
            self.updateUI()
        }
    }
    
    fileprivate func updateUI() {
        
        guard let level = self.viewModel?.level
            , let stages = self.viewModel?.stages else { return }
        
        self.title.text = NSLocalizedString("level", comment:"") + String(level)
        
        self.dataSource?.data = stages
        self.collectionView.reloadData()
        
    }
    
    fileprivate var dataSource:LevelDataSource? = {
        return LevelDataSource()
    }()
    
    //  MARK: - setup layout
    
    func setupSubviews() {
        self.addSubview(self.title)
        self.addSubview(self.collectionView)
    }
    
    func setupAutolayout() {
        
        constrain(self.title) { view in
            view.centerX == view.superview!.centerX
            view.top == view.superview!.top + 10
        }
        
        constrain(self.collectionView, self.title) { view,view2 in
            view.top == view2.bottom + 30
            view.width == view.superview!.width
            view.bottom == view.superview!.bottom - 30
        }
    }

}

extension LevelRootView:UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let stages = self.viewModel?.stages, stages.count > indexPath.item {
            self.delegate?.levelRootDidSelect(stages[indexPath.item])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 40)/3 - 20 , height: collectionView.frame.height/3 - 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 20, 0, 20)
    }
    
}
