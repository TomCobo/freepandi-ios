//
//  LevelCollectionViewCell.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 07/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

class LevelCollectionViewCell: BaseCollectionViewCell {
    
    //  MARK: - subviews
    
    fileprivate lazy var title: UILabel = {
        return FreePandiTheme.sharedManager.commonTitleLabel
    }()
    
    fileprivate lazy var lock: UIImageView = {
        return FreePandiTheme.sharedManager.lockGrey
    }()
    
    fileprivate lazy var stars: Array<UIImageView> = {
        var stars = Array<UIImageView>()
        for index in 0...2 {
            stars.append(FreePandiTheme.sharedManager.star)
        }
        return stars
    }()
    
    //  MARK: - setup layout
    
    override func setupSubviews() {
        self.addSubview(self.title)
        self.addSubview(self.lock)
        for star in self.stars.reversed() {
            self.addSubview(star)
        }
    }
    
    override func setupAutolayout() {
        constrain(self.lock) { view in
            view.center == view.superview!.center
        }
        
        for (index, star) in self.stars.enumerated() {
            constrain(star) { view in
                view.leading == view.superview!.leading - 10
                view.bottom == view.superview!.bottom - CGFloat(index * 28 + 5)
            }
        }
        
    }
    
    //  MARK: - data
    
    var data:Stage? {
        didSet {
            self.updateUI()
        }
    }
    
    fileprivate func updateUI() {
        
        self.lock.image = self.data?.status.image
        
        guard var numberOfStars = self.data?.numberStars else { return }
        
        numberOfStars = 3
        
        for (index,star) in self.stars.enumerated() {
            star.isHidden = index + 1 > numberOfStars
        }
        
        self.animateCurrentLevelLock()
        
        self.animateStars()
    }
    
    fileprivate func animateCurrentLevelLock() {
        if self.data?.status == StageStatus.currentLevel {
            self.rotateAndScaleAnimation(withScale:CGFloat(1.05) , rotation: CGFloat(M_PI_4/9))
        }
    }
    
    fileprivate func animateStars() {
        for star in self.stars {
            star.rotateAnimation(CGFloat(M_PI))
        }
    }
    
    override func prepareForReuse() {
        self.lock.image = UIImage(named: AssetIdentifier.LockGrey)
        for star in self.stars {
            star.isHidden = true
        }
    }
    
}
