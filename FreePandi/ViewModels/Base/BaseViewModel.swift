//
//  BaseViewModel.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

protocol BaseViewModelDelegate : class {
    func viewModelLoaded(_ viewModel: BaseViewModel)
}

class BaseViewModel {
    
    weak var delegate: BaseViewModelDelegate?
    
    func loadData() {
        
    }
    
    func viewModelLoaded() {
        self.delegate?.viewModelLoaded(self)
    }

}
