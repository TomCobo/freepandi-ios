//
//  LevelDataSource.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 07/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

class LevelDataSource:NSObject, UICollectionViewDataSource {
    
    var data:[Stage]?
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! LevelCollectionViewCell
        cell.data = self.data?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data?.count ?? 0
    }
    
}
