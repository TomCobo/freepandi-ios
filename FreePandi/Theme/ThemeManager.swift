//
//  ThemeManager.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

protocol ThemeInterface {
    
    // MARK: - Common colours
    
    var commonBlue:     UIColor { get }
    var commonBlueA100: UIColor { get }
    
    var commonPink:     UIColor { get }
    var commonPinkA200: UIColor { get }
    
    // MARK: - Common Fonts
    var commonGrandHotelRegular:String { get }
    
    // MARK: - Common views
    var commonTitleLabel:UILabel { get }
    
    //  MARK: - Images
    
    var verticalJailBar:UIImageView { get }
    var horizontalJailBar:UIImageView { get }
    
    var pandiBody:UIImageView { get }
    var pandiHead:UIImageView { get }
    
    var waves1:UIImageView { get }
    var waves2:UIImageView { get }
    
    var trafficLightGreen:UIImageView { get }
    var trafficLightAmbar:UIImageView { get }
    
    var lockGreen:UIImageView { get }
    var lockGrey:UIImageView { get }
    var lockRed:UIImageView { get }
    
    var star:UIImageView { get }
    var heart:UIImageView { get }
    
    var clouds1:UIImageView { get }
    var clouds2:UIImageView { get }
    
    // MARK: - Initial View
    
    var playButton:UIButton { get }
}

class FreePandiTheme:NSObject, ThemeInterface {
    
    static let sharedManager = FreePandiTheme()
    
    // MARK: - Common colours
    
    var commonBlue:     UIColor { return UIColor(rgb:0x2196F3) }
    var commonBlueA100: UIColor { return UIColor(rgb:0x82B1FF) }
    
    var commonPink:     UIColor { return UIColor(rgb:0xE91E63) }
    var commonPinkA200: UIColor { return UIColor(rgb:0xFF4081) }
    
    var commonGrandHotelRegular:String { return "GrandHotel-Regular" }
    
    // MARK: - Common Views
    
    var commonTitleLabel:UILabel {
        let commonTitleLabel = UILabel()
        commonTitleLabel.textColor = UIColor.white
        commonTitleLabel.shadowColor = self.commonPink
        commonTitleLabel.font = UIFont(name: self.commonGrandHotelRegular, size: 60)
        commonTitleLabel.shadowOffset = CGSize(width: 1.0, height: -1.0)
        return commonTitleLabel
    }
    
    //  MARK: - Images
    
    var verticalJailBar:UIImageView     { return UIImageView(assetIdentifier: AssetIdentifier.JailBar) }
    var horizontalJailBar:UIImageView   { return UIImageView(assetIdentifier: AssetIdentifier.JailFrame) }
    
    var pandiBody:UIImageView           { return UIImageView(assetIdentifier: AssetIdentifier.PandiBody) }
    var pandiHead:UIImageView           { return UIImageView(assetIdentifier: AssetIdentifier.PandiHead) }
    
    var waves1:UIImageView              { return UIImageView(assetIdentifier: AssetIdentifier.Waves1) }
    var waves2:UIImageView              { return UIImageView(assetIdentifier: AssetIdentifier.Waves2) }
    
    var trafficLightGreen:UIImageView   { return UIImageView(assetIdentifier: AssetIdentifier.TrafficLightGreen) }
    var trafficLightAmbar:UIImageView   { return UIImageView(assetIdentifier: AssetIdentifier.TrafficLightAmbar) }
    
    var lockGreen:UIImageView           { return UIImageView(assetIdentifier: AssetIdentifier.LockGreen) }
    var lockGrey:UIImageView            { return UIImageView(assetIdentifier: AssetIdentifier.LockGrey) }
    var lockRed:UIImageView             { return UIImageView(assetIdentifier: AssetIdentifier.LockRed) }
    
    var star:UIImageView                { return UIImageView(assetIdentifier: AssetIdentifier.Star) }
    var heart:UIImageView               { return UIImageView(assetIdentifier: AssetIdentifier.Heart) }
    
    var clouds1:UIImageView             { return UIImageView(assetIdentifier: AssetIdentifier.Clouds1)}
    var clouds2:UIImageView             { return UIImageView(assetIdentifier: AssetIdentifier.Clouds2) }
    
    // MARK: - Initial View
    
    var playButton:UIButton {
        let playButton = UIButton(type: UIButtonType.custom)
        playButton.setTitle("Play", for: UIControlState())
        playButton.titleLabel?.font = UIFont(name: self.commonGrandHotelRegular, size: 40)
        playButton.backgroundColor = self.commonPinkA200
        playButton.layer.borderColor = self.commonBlue.cgColor
        playButton.layer.borderWidth = 5
        playButton.layer.cornerRadius = 50
        playButton.clipsToBounds = true
        return playButton
    }
    
    
}
