//
//  ImagesNames.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 13/02/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

struct AssetIdentifier {
    static let PandiHead  = "pandi_head"
    static let PandiBody  = "pandi_body"
    static let JailBar    = "jail_bar"
    static let JailFrame  = "jail_frame"
    static let Waves1     = "waves_1"
    static let Waves2     = "waves_2"
    static let TrafficLightGreen     = "traffic_light_green"
    static let TrafficLightAmbar     = "traffic_light_ambar"
    static let Star       = "star"
    static let LockRed    = "lock_red"
    static let LockGrey   = "lock_grey"
    static let LockGreen  = "lock_green"
    static let Heart      = "heart"
    static let Clouds1    = "clouds_1"
    static let Clouds2    = "clouds_2"
}