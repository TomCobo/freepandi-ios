//
//  Quiz.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

struct Quiz {
    
    var levels:Array<Level>?
    
    init(withObjects objects:Array<AnyObject>?) {
        
        guard let levelsObjects = objects as? [Dictionary<String,AnyObject>] else { return }
        
        self.levels = Array()
        
        for obj in levelsObjects {
            self.levels?.append(Level(withDictionary: obj))
        }
    }
    
}
