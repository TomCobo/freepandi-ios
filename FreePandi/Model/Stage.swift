//
//  File.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum StageStatus {
    
    case locked
    case unlocked
    case currentLevel
    
    var image: UIImage {
        switch self {
            case .locked: return UIImage(named: AssetIdentifier.LockGrey)!
            case .unlocked: return UIImage(named: AssetIdentifier.LockGreen)!
            case .currentLevel: return UIImage(named:AssetIdentifier.LockRed)!
        }
    }
}

struct Stage {
    
    var level:Int?
    var stage:Int?
    var digits:Int?
    var time:TimeInterval?
    var attempts:Int?
    
    // MARK: - Score & stars
    
    var score:Int? = 0
    
    var numberStars:Int {
        guard let score = self.score else { return 0 }
        return score/25000
    }
    
    var status:StageStatus {
        if let score = self.score, score > 0 && self.hasBeenUnlock {
            return StageStatus.unlocked
        }
        return self.isCurrentLevel ? StageStatus.currentLevel : StageStatus.locked
    }
    
    fileprivate var isCurrentLevel:Bool {
        return User.sharesInstance.currentLevelStage.0 == stage && User.sharesInstance.currentLevelStage.1 == level
    }
    
    fileprivate var hasBeenUnlock:Bool {
        return User.sharesInstance.currentLevelStage.0 > stage && User.sharesInstance.currentLevelStage.1 > level
    }
    
    init(withDictionary dictionaryStage:Dictionary<String,AnyObject>?) {
        
        guard (dictionaryStage != nil) else { return }

        self.stage = dictionaryStage?["stage"] as? Int
        self.digits = dictionaryStage?["digits"] as? Int
        self.time = dictionaryStage?["time"] as? TimeInterval
        self.attempts = dictionaryStage?["attempts"] as? Int
    }
}
