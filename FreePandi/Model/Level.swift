//
//  Level.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 20/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

struct Level {
    
    var level:Int?
    var stages:[Stage]? = []
    
    init(withDictionary dictionaryLevel:Dictionary<String,AnyObject>?) {
        
        guard (dictionaryLevel != nil) else { return }
        
        guard let stagesDictionaries = dictionaryLevel!["stages"] as? Array<Dictionary<String,AnyObject>> else { return }
        
        self.level = dictionaryLevel!["level"] as? Int

        for stage in stagesDictionaries {
            var stage = Stage(withDictionary: stage)
            stage.level = self.level
            self.stages?.append(stage)
        }
    }
}