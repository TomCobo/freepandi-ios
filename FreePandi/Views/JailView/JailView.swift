//
//  JailView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import Cartography

class JailView: BaseView {
    
    fileprivate var verticalBarsCenterConstraint = Array<NSLayoutConstraint>()
    fileprivate var horizontalBarsCenterConstraint = Array<NSLayoutConstraint>()
    fileprivate var isJailOpen:Bool
    
    
    //  MARK: - subviews

    fileprivate lazy var horizontalBars:Array<UIImageView> = {
        var horizontalBars = Array<UIImageView>()
        for _ in 0...1 {
            horizontalBars.append(FreePandiTheme.sharedManager.horizontalJailBar)
        }
        return horizontalBars
    }()
    
    fileprivate lazy var verticalBars:Array<UIImageView> = {
        var verticalBars = Array<UIImageView>()
        for _ in 0...4 {
            verticalBars.append(FreePandiTheme.sharedManager.verticalJailBar)
        }
        return verticalBars
    }()

    init(withJailOpened opened:Bool = true) {
        self.isJailOpen = opened
        super.init(frame:CGRect.zero)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviews() {

        for view in self.verticalBars {
            self.addSubview(view)
        }
        
        for view in self.horizontalBars {
            self.addSubview(view)
        }

    }
    
    func setupAutolayout() {

        // Horizontal View layout
        
        for (index, viewBar) in self.horizontalBars.enumerated() {
        
            constrain(viewBar,self.verticalBars[index+3]) { view1,view2 in
                view1.centerX == view1.superview!.centerX
                
                var constrainHorizontalBar:NSLayoutConstraint!
                if index == 0 {
                    constrainHorizontalBar = view1.bottom  == view2.bottom + DeviceUtils.screenHeight
                } else {
                    constrainHorizontalBar = view1.top  == view2.top - DeviceUtils.screenHeight
                }
                self.horizontalBarsCenterConstraint.append(constrainHorizontalBar)
            }
            
        }
        
        // Vertical View layout
        
        let verticalBarWidth = self.verticalBars.first!.frame.width
        let horizontalBarWidth:CGFloat =  260.0 //self.horizontalBars.first!.frame.width
        let leadingIndexOffset = verticalBarWidth + (horizontalBarWidth/6) + 1
        
        for (index, viewBar) in self.verticalBars.enumerated() {
            
            let leadingVerticalBarOffset = 2 + (CGFloat(index) * leadingIndexOffset)
            let centerYVerticalBarOffset = (index%2 == 0 ? -DeviceUtils.screenHeight : DeviceUtils.screenHeight)
            
            let centerYVerticalBarOffsetOpen = self.isJailOpen ? centerYVerticalBarOffset : -60
            
            constrain(viewBar) { view in
                
                let viewContraint = view.centerY  == view.superview!.centerY + centerYVerticalBarOffsetOpen
                self.verticalBarsCenterConstraint.append(viewContraint)
                
                view.leading == view.superview!.leading + leadingVerticalBarOffset

            }
        }
        
    }
    
    //  MARK: - public animations

    func toggleJail() {
        self.animateToggleCellBars()
    }
    
    func closeJail(_ completion: ((Bool) -> Void)? = nil) {
        if self.isJailOpen == false { return }
        self.animateVerticalBars { (finished) -> Void in
            if finished {
                self.animateHorizontalBars({ (finished) -> Void in
                    
                    completion?(true)
                })
            }
        }
    }
    
    func openJail(_ completion: ((Bool) -> Void)? = nil) {
        
        if self.isJailOpen { return }
        
        self.animateHorizontalBars { (finished) -> Void in
            if finished {
                self.animateVerticalBars({ (finished) -> Void in
                    completion?(true)
                })
            }
        }
    }
    
    fileprivate func animateToggleCellBars(_ completion: ((Bool) -> Void)? = nil) {
        self.animateVerticalBars { (finished) -> Void in
            if finished {
                self.animateHorizontalBars({ (finished) -> Void in
                    completion?(true)
                })
            }
        }
    }
    
    fileprivate func animateHorizontalBars(_ completion:((Bool) -> Void)? = nil) {

        for (index, viewConstraint) in self.horizontalBarsCenterConstraint.enumerated() {
            
            let openOffset = index == 0 ? DeviceUtils.screenHeight : -DeviceUtils.screenHeight
            
            viewConstraint.constant = self.isJailOpen ? 0 : openOffset
            
            self.horizontalBars[index].animateConstraintWithDuration(options: .curveEaseOut, completion: { (finished) -> Void in
                if index == 1 {
                    completion?(true)
                    self.isJailOpen = !self.isJailOpen
                }
            })
        }
    }
    
    fileprivate func animateVerticalBars(_ completion:((Bool) -> Void)? = nil) {
        for (index, viewConstraint) in self.verticalBarsCenterConstraint.enumerated() {
            
            let centerYVerticalBarOffset = (index%2 == 0 ? -DeviceUtils.screenHeight : DeviceUtils.screenHeight)
            let centerYVerticalBarOffsetOpen = self.isJailOpen ? -60: centerYVerticalBarOffset
            
            viewConstraint.constant = centerYVerticalBarOffsetOpen
            self.animateVerticalBarAtIndex(index) { (finished) -> Void in
                if index == 0 {
                    completion?(true)
                }
            }
        }
    }
    
    fileprivate func animateVerticalBarAtIndex(_ index:Int,completion:((Bool) -> Void)? = nil) {
        let delay = (index%2 == 0) ? index : max(0, index-1)
        self.verticalBars[index].animateConstraintWithDuration(delay:Double(delay) * 0.05 , options: .curveEaseOut, completion:completion)
    }
    
}
