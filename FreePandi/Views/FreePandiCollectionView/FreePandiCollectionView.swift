//
//  FreePandiCollectionView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 21/02/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

class FreePandiCollectionView: BaseView, SetupView {
    
    var dataSource:UICollectionViewDataSource?
    var delegate:UICollectionViewDelegate?
    
    //  MARK: - init
    
    init(withDataSource dataSource:UICollectionViewDataSource?, andDelegate delegate:UICollectionViewDelegate?) {
        super.init(frame:CGRect.zero)
        self.dataSource = dataSource
        self.delegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    //  MARK: - public methods
    
    func registerClass(_ classObject:AnyClass?) {
        self.collectionView.register(classObject, forCellWithReuseIdentifier: "cell")
    }
    
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    //  MARK: - Subviews
    
    fileprivate lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = UICollectionViewScrollDirection.vertical
        return collectionViewLayout
    }()
    
    fileprivate lazy var collectionView: UICollectionView = {
        let collectionView:UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.collectionViewLayout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.dataSource = self.dataSource
        collectionView.delegate = self.delegate
        return collectionView
    }()
    
    //  MARK: - setup layout
    
    func setupSubviews() {
        self.addSubview(self.collectionView)
    }
    
    func setupAutolayout() {
        constrain(self.collectionView) { view in
            view.center == view.superview!.center
            view.size == view.superview!.size
        }
    }

}
