//
//  BaseView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 16/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import UIKit

protocol ConfigureView:class {
    func configure()
}

protocol SetupView:class {
    func setupSubviews()
    func setupAutolayout()
}

class BaseView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
