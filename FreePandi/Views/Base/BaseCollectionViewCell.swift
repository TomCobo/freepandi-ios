//
//  BaseCollectionViewCell.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 07/03/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
        self.setupAutolayout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubviews() {
        
    }
    
    func setupAutolayout() {
        
    }

}
