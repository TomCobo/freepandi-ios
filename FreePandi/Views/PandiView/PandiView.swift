//
//  PandiView.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit
import Cartography

class PandiView: BaseView {

    //  MARK: - subviews
    
    fileprivate lazy var pandiBody:UIImageView = {
        let pandiBody = FreePandiTheme.sharedManager.pandiBody
        return pandiBody
    }()
    
    fileprivate lazy var pandiHead:UIImageView = {
        let pandiHead = FreePandiTheme.sharedManager.pandiHead
        return pandiHead
    }()
    
    func setupSubviews() {
        self.addSubview(self.pandiBody)
        self.addSubview(self.pandiHead)
    }
    
    func setupAutolayout() {
        
        constrain(self.pandiBody) { view in
            view.center == view.superview!.center
        }

        constrain(self.pandiHead, self.pandiBody) { view1,view2 in
            view1.centerX == view2.centerX
            view1.centerY  == view2.centerY - 40
        }
        
    }
    
    //  MARK: - public animations
    
    func rotatePandiToDefaultPosition() {
//        self.rotatePandiHead(0)
    }
    
    func rotatePandiHeadRigth() {
//        self.rotatePandiHead(20)
    }
    
    fileprivate func rotatePandiHead(_ degrees:CGFloat) {
        
        let rotationDegrees:CGFloat = degrees * CGFloat(M_PI/180.0)
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.pandiHead.transform = CGAffineTransform(rotationAngle: rotationDegrees)
            }, completion: nil)

    }

}
