//
//  NavigationService.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit


enum ViewController : String {
    case Main    = "MainViewController"
    case Home    = "HomeViewController"
    case Initial = "InitialViewController"
    case Levels  = "LevelsViewController"
    case Stage  = "StageViewController"
}

protocol NavigationServiceDelegate : class {
    func navigationServiceNavigatedTo(_ contentController: BaseViewController)
}

typealias BaseViewControllerFactory = ObjectFactory<BaseViewController>

class NavigationService: NSObject {
    
    static let sharedService =  NavigationService()
    
    // MARK: - variables
    weak var delegate: NavigationServiceDelegate?
    
    fileprivate lazy var navigationController:UINavigationController? = {
        let navigationController = UINavigationController(rootViewController: InitialViewController())
        navigationController.setNavigationBarHidden(true, animated: false)
        return navigationController
    }()
    
    func initialise() -> UINavigationController? {
        return self.navigationController
    }
    
    // MARK: - methods
    
    func setRootViewController(_ controller: ViewController, data: AnyObject? = nil, animated:Bool = true) {
        let vc = viewControllerForNameWithData(controller, data: data)
        self.navigationController?.setViewControllers([vc], animated: animated)
        self.delegate?.navigationServiceNavigatedTo(vc)
    }
    
    func pushViewController(_ controller: ViewController, data: AnyObject? = nil, animated:Bool = true) -> BaseViewController {
        let vc = viewControllerForNameWithData(controller, data: data)
        self.navigationController?.pushViewController(vc, animated: animated)
        self.delegate?.navigationServiceNavigatedTo(vc)
        return vc
    }
    
    func presentViewController(_ controller: ViewController, data: AnyObject? = nil, animated:Bool = true) -> BaseViewController {
        let vc = viewControllerForNameWithData(controller, data: data)
        self.navigationController?.present(vc, animated: animated) { () -> Void in
            self.delegate?.navigationServiceNavigatedTo(vc)
        }
        return vc
    }
    
    func pushViewController(_ controller: BaseViewController, animated:Bool = true) {
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func presentViewController(_ viewController:BaseViewController, animated:Bool = true) {
        self.navigationController?.present(viewController, animated: animated, completion: { () -> Void in
            self.delegate?.navigationServiceNavigatedTo(viewController)
        })
    }
    
    fileprivate func viewControllerForNameWithData(_ controller: ViewController, data: AnyObject? = nil) -> BaseViewController {
        
        let className = "FreePandi.\(controller.rawValue)"
        
        var vc : BaseViewController?
        
        if data == nil {
            vc = BaseViewControllerFactory.createInstance(className: className)
        } else {
            vc = BaseViewControllerFactory.createInstance(className: className , initializer: #selector(BaseViewController.init(params:)), argument: data!)
        }
        
        vc?.viewControllerType = controller
        
        return vc!
    }
    
}
