//
//  DeviceUtils.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

class DeviceUtils: NSObject {
    
    static let screenHeight =  DeviceUtils.screenSize().height
    static let screenWidth =  DeviceUtils.screenSize().width
    
    class func isDeviceIPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    
    class func isDeviceIPhone() -> Bool {
        return !DeviceUtils.isDeviceIPad()
    }
    
    class func isDeviceOrientationLandscape() -> Bool {
        return  UIApplication.shared.statusBarOrientation.isLandscape
    }
    
    class func screenSize() -> CGSize {
        let screenBounds: CGRect = UIScreen.main.bounds
        
        let screenSize = DeviceUtils.isDeviceOrientationLandscape() && !isIOS8() ?
            CGSize(width: screenBounds.size.height, height: screenBounds.size.width) :
            CGSize(width: screenBounds.size.width, height: screenBounds.size.height)
        
        return screenSize
    }
    
    class func isIOS8() -> Bool {
        return UIDevice.current.systemVersion.range(of: "8.") != nil
    }
    
}
