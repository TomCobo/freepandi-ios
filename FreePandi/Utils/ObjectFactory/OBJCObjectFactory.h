//
//  OBJCObjectFactory.h
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

@import Foundation;

/** Instantiates NSObject subclasses. */
@interface OBJCObjectFactory : NSObject

/**
 Instantiates the specified class, which must
 descend (dircectly or indirectly) from NSObject.
 Uses the class's parameterless initializer.
 */
+ (id)create:(NSString *)className;

/**
 Instantiates the specified class, which must
 descend (dircectly or indirectly) from NSObject.
 Uses the specified initializer, passing it the
 argument provided via the `argument` parameter.
 */
+ (id)create:(NSString *)className
 initializer:(SEL)initializer
    argument:(id)argument;


@end
