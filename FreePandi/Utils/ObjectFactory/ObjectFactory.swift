//
//  ObjectFactory.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 23/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

class ObjectFactory<TBase: NSObject> {
    
    class func createInstance(className: String!) -> TBase? {
        return OBJCObjectFactory.create(className) as! TBase?
    }

    class func createInstance( className:  String!, initializer: Selector!, argument:    AnyObject) -> TBase? {
        return OBJCObjectFactory.create( className, initializer: initializer, argument: argument) as! TBase?
    }
    
}
