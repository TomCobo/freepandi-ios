//
//  DataManager.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 06/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    case empty
    case noFile
    case serialize
    case unknown
}

extension DataManagerError: CustomStringConvertible {
    var description: String {
        let dataManagerError:String
        switch self {
        case .empty: dataManagerError = "no data on file found"
        case .noFile: dataManagerError = "no file found"
        case .serialize: dataManagerError = "serialization"
        case .unknown: dataManagerError = "unknown"
        }
        return "Error DataManager: " + dataManagerError
    }
}

class DataManager {
    
    class func deserializeJSONFile(_ fileName:String, responseData:((_ data: Array<AnyObject>?) -> Void)) {
        
        var resultData:Array<AnyObject>?
        
        defer {
            responseData(resultData)
        }
        
        do {
            guard let filePath = Bundle.main.path(forResource: fileName,ofType:"json") else {
                throw DataManagerError.noFile
            }
            
            guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath), options:NSData.ReadingOptions.uncached) else {
                throw DataManagerError.serialize
            }
            
            guard let results =  try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                throw DataManagerError.empty
            }
            
            resultData = results as? Array
        }
        catch let error as DataManagerError {
            print(error.description)
        }
        catch {
            print(DataManagerError.unknown.description)
        }
        
    
    }
    
}
