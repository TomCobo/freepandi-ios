//
//  UIViewExtensions.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 31/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

extension UIView {
    
    func  animateConstraintWithDuration(_ duration: TimeInterval = 0.6,
        delay: TimeInterval = 0.0,
        options: UIViewAnimationOptions = UIViewAnimationOptions(),
        completion: ((Bool) -> Void)? = nil) {
            
        UIView.animate(withDuration: duration, delay:delay, options:options, animations: { [weak self] in
            self?.superview?.layoutIfNeeded() ?? ()
            }, completion: completion)
            
    }
    
    func bounceAnimation(withScale scale:CGFloat) {
        
        UIView.animate(withDuration: 2.4,
            delay: 0.0,
            usingSpringWithDamping: 0.1,
            initialSpringVelocity: 1.2,
            options:    [UIViewAnimationOptions.allowUserInteraction,
                UIViewAnimationOptions.repeat,
                UIViewAnimationOptions.beginFromCurrentState,
                UIViewAnimationOptions.autoreverse],
            animations: { [unowned self] in
                self.transform =  CGAffineTransform(scaleX: scale, y: scale)
            }, completion: nil)
    }
    
    func rotateAndScaleAnimation(withScale scale:CGFloat, rotation:CGFloat) {
        
        UIView.animate(withDuration: 0.6,
            delay: 0.0,
            usingSpringWithDamping: 0.4,
            initialSpringVelocity: 2.4,
            options:    [UIViewAnimationOptions.allowUserInteraction,
                        UIViewAnimationOptions.repeat,
                        UIViewAnimationOptions.beginFromCurrentState,
                        UIViewAnimationOptions.autoreverse],
            animations: { [unowned self] in
                self.transform  = CGAffineTransform(scaleX: scale, y: scale).rotated(by: rotation)
            }, completion: nil)
    }
    
    func rotateAnimation(_ rotation:CGFloat) {
        
        UIView.animate(withDuration: 0.6, delay: 0.0, options: [ UIViewAnimationOptions.beginFromCurrentState], animations: { () -> Void in
            self.transform  = CGAffineTransform(rotationAngle: rotation)
            }, completion: nil)
    }
    
    
    
}
