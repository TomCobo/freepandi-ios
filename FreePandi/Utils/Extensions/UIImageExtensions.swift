//
//  UIImageExtensions.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 17/01/2016.
//  Copyright © 2016 Tomas Cobo Martinez. All rights reserved.
//

import UIKit

extension UIImageView {
    convenience init!(assetIdentifier: String) {
        self.init(image: UIImage(named: assetIdentifier))
    }
}