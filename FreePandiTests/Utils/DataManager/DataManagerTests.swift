//
//  DataManagerTests.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 06/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import XCTest

@testable import FreePandi

class DataManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFileNotFound() {
        DataManager.deserializeJSONFile("thisFileDoesntExit") { XCTAssertNil($0, "Error: should be empty") }
    }
    
    func testFileDeserialationFailed() {
        DataManager.deserializeJSONFile("levelFail") { XCTAssertNil($0, "Error: should be empty") }
    }
    
    func testFileDeserialationSuccess() {
        DataManager.deserializeJSONFile("level") { XCTAssertNotNil($0, "Error: should have data") }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
