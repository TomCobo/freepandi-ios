//
//  CommonDataTests.swift
//  FreePandi
//
//  Created by T Cobo Martinez on 22/12/2015.
//  Copyright © 2015 Tomas Cobo Martinez. All rights reserved.
//

import Foundation
import XCTest

@testable import FreePandi

class CommonDataTests: XCTestCase {
    
    var createUniqueInstanceCommonData:CommonData {
        return CommonData()
    }
    
    var commonDataShared:CommonData {
        return CommonData.sharedData
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    //  MARK: - Test CommonData Singleton
    
    func testUniqueCommonDataSharedInstanceCreated() {
        XCTAssertNotNil(self.commonDataShared, "Error: commonData not created")
    }
    
    func testUniqueCommonDataInstanceCreated() {
        XCTAssertNotNil(self.createUniqueInstanceCommonData, "Error: commonData not created")
    }
    
    func testUniqueCommonDataReturnsSameSharedInstanceTwice() {
        XCTAssertEqual(self.commonDataShared, self.commonDataShared, "Error: commonData shared is different")
    }
    
    func testCommonDataSharedInstanceSeparateFromUniqueInstance() {
        XCTAssertNotEqual(self.commonDataShared, self.createUniqueInstanceCommonData);
    }
    
    func testCommonDataSharedInstanceReturnsSeparateUniqueInstances() {
        XCTAssertNotEqual(self.createUniqueInstanceCommonData, self.createUniqueInstanceCommonData);
    }
    
    //  MARK: -data
    
    func testCommonDataCreateQuiz() {
        XCTAssertNotNil(self.commonDataShared.quiz,"Error: common data empty")
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
